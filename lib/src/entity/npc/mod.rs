/*
 *  BoardTracker
 *  Copyright (C) 2016 Jakob Sinclair
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Jakob Sinclair <sinclair.jakob@openmailbox.org>
 */

use std::string::String;
use entity::statistics::Statistics;

pub struct Monster {
    pub name: String,
    pub tag: String,
    pub stats: Statistics,
}

impl Monster {
    pub fn new(t: String, m: &Monster) -> Monster {
        Monster {
            name: m.name.clone(),
            tag: t,
            stats: m.stats, 
        }
    }

    pub fn new_type(n: String, t: String, h: i32, s: i32, d: i32, c: i32, i: i32, w: i32,
                    ch: i32) -> Monster {
        Monster {
            name: n,
            tag: t,
            stats: Statistics {
                hp: h,
                max_hp: h,

                strenght: s,
                dexterity: d,
                constitution: c,
                intelligence: i,
                wisdom: w,
                charisma: ch,
            }
        }
    }
}
