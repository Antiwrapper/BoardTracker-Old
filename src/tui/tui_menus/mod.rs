/*
 *  BoardTracker
 *  Copyright (C) 2016 Jakob Sinclair
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Jakob Sinclair <sinclair.jakob@openmailbox.org>
 */

use bt_lib::entity::player::Player;
use cursive::Cursive;
use cursive::views::*;
use cursive::traits::*;
use cursive::ScreenId;
use tui;
use std;

pub fn setup_ui(screens: (ScreenId, ScreenId, ScreenId, ScreenId, ScreenId), ui: &mut Cursive) {
    let (menu_screen, create_party_screen, add_members_screen, create_member_screen, data) = screens;
	
	setup_data(ui, data);
	setup_main_menu(ui, menu_screen, create_party_screen);
	setup_create_party_menu(ui, create_party_screen, menu_screen, add_members_screen, data);
	setup_add_members(ui, add_members_screen, create_member_screen, create_party_screen, data);
	setup_create_member_menu(ui, create_member_screen, add_members_screen, data);

	
	ui.set_screen(menu_screen);
}

fn setup_data(ui: &mut Cursive, data: ScreenId) {
	ui.set_screen(data);
	ui.add_layer(EditView::new()
        .content("Party name")
        .with_id("party_name"));
} 

fn setup_main_menu(ui: &mut Cursive, menu_screen: ScreenId, create_party_screen: ScreenId) {
	ui.set_screen(menu_screen);

    ui.add_layer(Dialog::text("Welcome! Please choose an option!")
		.title("BoardTracker")
		.button("New game", move |s| s.set_screen(create_party_screen))
        .button("Load session", move |s| 
			s.add_layer(Dialog::text("Function not added!")
				.title("Error!")
				.button("Ok", move |s| s.pop_layer())))
        .button("Quit", move |s| s.quit()));
}

fn setup_create_party_menu(ui: &mut Cursive, create_party_screen: ScreenId, menu_screen: ScreenId, add_members_screen: ScreenId, data: ScreenId) {
	ui.set_screen(create_party_screen);
	
	ui.add_layer(Dialog::new()
        .title("Enter name for new party")
        .content(EditView::new()
            .with_id("name"))
        .button("Ok", move |s| {
            let name = s.find_id::<EditView>("name")
                .unwrap()
                .get_content();
			s.set_screen(data);
			{
				let ref mut dialog = s.find_id::<EditView>("party_name").unwrap();
				dialog.set_content(name.to_string());
			}

            s.set_screen(add_members_screen);
			{
				let ref mut dialog = s.find_id::<Dialog>("member_list").unwrap();
				dialog.set_title(name.to_string());
			}
        })
		.button("Cancel", move |s| s.set_screen(menu_screen)));
}

fn setup_add_members(ui: &mut Cursive, add_members_screen: ScreenId, create_member_screen: ScreenId, create_party_screen: ScreenId, data: ScreenId) {
	ui.set_screen(add_members_screen);

	let buttons = LinearLayout::vertical()
		.child(Button::new("Add new member", move |s| s.set_screen(create_member_screen)))
        .child(Button::new("Delete member", |s| {
			let dialog = s.find_id::<SelectView>("list").unwrap();
			let result = dialog.selected_id();
			if result.is_some() == true {
				let id = result.unwrap();
				dialog.remove_item(id);
			}
		}))
		.child(Button::new("Finished", move |s| {
			s.set_screen(data);
			let name = {
				let ref mut dialog = s.find_id::<EditView>("party_name").unwrap();
				let n = dialog.get_content();
				n
			};
			tui::gameloop(s, name.to_string());
		}))
        .child(DummyView)
		.child(Button::new("Cancel", move |s| s.set_screen(create_party_screen)));

    let player_list = SelectView::<Player>::new()
        .with_id("list")
        .fixed_size((20, 6));
                    
	ui.set_screen(add_members_screen);
		
    ui.add_layer(Dialog::around(
        LinearLayout::horizontal()
			.child(player_list)
            .child(DummyView)
            .child(buttons)
        ).with_id("member_list"));
}

fn setup_create_member_menu(ui: &mut Cursive, create_member_screen: ScreenId, add_members_screen: ScreenId, data: ScreenId) {
	ui.set_screen(create_member_screen);
	
	ui.add_layer(Dialog::new()
    .title("Character sheet")
    .button("Ok", move |s| {
		let cname = s.find_id::<EditView>("cname")
			.unwrap()
			.get_content();
		let pname = s.find_id::<EditView>("pname")
			.unwrap()
			.get_content();
	
		s.set_screen(data);
		s.add_layer(EditView::new().content(cname.to_string()).with_id("cname"));
		s.add_layer(EditView::new().content(pname.to_string()).with_id("pname"));
		s.set_screen(add_members_screen);
		let list = s.find_id::<SelectView<Player>>("list");
		if list.is_some() == true {
			let p = list.unwrap();
			p.add_item(pname.to_string(), Player::new(cname.to_string(), 10, 10, 10, 10, 10, 10, 10));
		}
	})
    .content(ListView::new()
		.child("Player name", EditView::new()
			.with_id("pname")
			.fixed_width(20))
        .child("Character name", EditView::new()
			.with_id("cname")
			.fixed_width(20))
        .child("Sex: Male / Female", LinearLayout::horizontal()
			.child(Checkbox::new()
				.on_change(|s, checked| {
					if checked == true {
						let ref mut dialog = s.find_id::<Checkbox>("female").unwrap();
						dialog.set_checked(false);
					}
				}).with_id("male"))
			.child(Checkbox::new()
				.on_change(|s, checked| {
					if checked == true {
						let ref mut dialog = s.find_id::<Checkbox>("male").unwrap();
						dialog.set_checked(false);
					}
				}).with_id("female")))
        .delimiter()
        .child("Age", EditView::new()
			.on_edit(move |ui, text, pos| {
				let dialog = ui.find_id::<EditView>("age").unwrap();
				dialog.set_content("apa");
			})
            .with_id("age"))
		.child("Race", SelectView::new()
			.popup()
			.item_str("Hill Dwarf")
			.item_str("Mountain Dwarf")
			.item_str("High Elf")
			.item_str("Wood Elf")
			.item_str("Dark Elf")
			.item_str("Halfling")
			.item_str("Human")
			.item_str("Dragonborn")
			.item_str("Forest Gnome")
			.item_str("Rock Gnome")
			.item_str("Half-Elf")
			.item_str("Half-Orc")
			.item_str("Tiefling"))
		.with_id("sheet")));
}