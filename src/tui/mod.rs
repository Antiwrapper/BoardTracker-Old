/*
 * BoardTracker
 * Copyright (C) 2016 Jakob Sinclair
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Jakob Sinclair <sinclair.jakob@openmailbox.org>
 */

mod tui_menus;

use cursive::Cursive;
use self::tui_menus::*;
use bt_lib::entity::party::Party;
use std::string::String;

pub fn start(party: &mut Party) {
    let mut ui = Cursive::new();
	setup_ui((ui.add_screen(), ui.add_screen(), ui.add_screen(), ui.add_screen(), ui.add_screen()), &mut ui);
    ui.run();
}

pub fn create_party(ui: &mut Cursive, party_name: &str) -> Party {
    let party = Party::new(party_name.to_string());
    party
}

fn gameloop(ui: &mut Cursive, party_name: String) {
    
}
